FROM golang:1.15 as build

WORKDIR /go/src/app
COPY . .
RUN go build -o analyzer

FROM debian:stable-slim

COPY --from=build /go/src/app/analyzer /analyzer

CMD ["/analyzer", "run"]
