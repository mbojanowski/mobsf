# MobSF analyzer changelog

## v2.0.0
- Finalize v2.0.0 release (!3)
- Mock project into MobSFs expected structure (!1)

## v1.0.0
- Initial beta release. Code provided by H-E-B (!2)
