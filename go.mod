module mobsf

go 1.15

require (
	github.com/sirupsen/logrus v1.6.0
	github.com/urfave/cli v1.22.4
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.15.0
)
